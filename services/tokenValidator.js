const jwt = require("jsonwebtoken");

module.exports = (cert) => (token, tempSession) => new Promise((resolve, reject) => {
  jwt.verify(token, cert, { algorithms: ["RS256"] }, (err, decoded) => {
    if(err)
      reject(err);
    else {
      // Check if nonce is known
      if (tempSession && decoded.nonce !== tempSession.nonce) {
        reject({status: 400, message: "Nonce returned is unknown"});
        return;
      }

      // Return
      resolve({
        decoded,
        user: {
          iss: decoded.iss,
          id: decoded.sub,
          legacy_id: decoded.legacy_id,
          name: {
            full: decoded.name,
            familyName: decoded.family_name,
            givenName: decoded.given_name,
          },
          email: decoded.email,
          roles:
            decoded.realm_access && decoded.realm_access
              ? decoded.realm_access.roles
              : [],
        }
      });
    }
  });
});