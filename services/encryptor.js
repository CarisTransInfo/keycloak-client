const encryptionModule = require('encryption-se');

module.exports = (password, passwordSalt = 'C5mp4Hl$X9wby#s5', iterationCount = 123123) => {
  const encryption = encryptionModule({ password, passwordSalt, iterationCount });

  return {
    decrypt: enc => encryption.decrypt(enc),
    encrypt: user => encryption.encrypt(JSON.stringify(user))
  }
};
