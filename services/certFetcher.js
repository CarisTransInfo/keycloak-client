const axios = require('axios');
const jwkToPem = require('jwk-to-pem');

module.exports = async (jwksUri) => {
  const response = await axios.get(jwksUri);
  return jwkToPem(response.data.keys[0]);
}
