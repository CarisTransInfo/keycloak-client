module.exports = (router, client, tokenValidator, options) => {

  // Cache Init
  const NodeCache = require('node-cache');
  const cache = new NodeCache();

  // Add cookieParser
  require('./cookieParser')(router);

  // Send to SSO login
  require('./login')(router,
    client,
    options.paths?.login || '/login',
    options.paths?.sso || '/sso',
    cache
  );

  // Remove session
  require('./logout')(router,
    client,
    options.paths?.logout || '/logout',
    options.paths?.afterLogout || '/'
  );

  // Handle return from SSO (first-jump)
  require('./sso1')(router, options.paths?.sso || '/sso');

  // Encryptor
  const encryptor = require('./../services/encryptor')(
    options.encPassword,
    options.encPasswordSalt,
    options.encIterationCount
  );

  // Handle return from SSO (second-jump)
  require('./sso2')(
    router,
    tokenValidator,
    encryptor,
    options.paths?.sso || '/sso',
    options.paths?.afterLogin || '/',
    cache);

  // Inject user from session cookie if present
  require('./user')(router, encryptor);
}
