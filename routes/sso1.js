module.exports = (router, path) => {
  router.get(path, (req, res, next) => {
    if(req.originalUrl === '/sso') {
      res.writeHead(200, { 'Content-Type': 'text/html' });
      res.write('<html><head><script type="application/javascript">window.location.href = \'?\' + window.location.href.split(\'#\')[1];</script></head><body>Redirecting</body></html>');
      res.end();
    }
    else
      next();
  });
};
