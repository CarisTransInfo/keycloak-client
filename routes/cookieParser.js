const cookieParser = require('cookie-parser');
module.exports = (router) => {
  router.all('/*', (req, res, next) => {
    if (req.cookie)
      next(); // Cookie parser is already attached
    else
      cookieParser()(req, res, next);  // Cookie parser is needed
  });
}
