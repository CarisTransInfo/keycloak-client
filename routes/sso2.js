module.exports = (router, tokenValidator, encryptor, ssoPath, afterLoginPath, cache) => {
  router.get(ssoPath, (req, res, next) => {
    // Prevent step-1 to proceed
    if(req.originalUrl === '/sso') {
      next();
      return;
    }

    // Check do we have some session available
    if(!req.cookies['x-session-temp'] || !cache.has(req.cookies['x-session-temp'])) {
      next({ status: 401, message: 'Response received, but nonce not available locally' });
      return;
    }

    // Retrieve them session and remove
    const tempSession = cache.take(req.cookies['x-session-temp']);

    // We have token, lets validate it and add encrypted to a session cookie
    tokenValidator(req.query.access_token, tempSession)
      .then(({ user, decoded }) => user)
      .then(encryptor.encrypt)
      .then((session) => {
        res.cookie('x-session-temp', 'Removing...', { httpOnly: true, maxAge: 0 });
        res.cookie('x-session', session, { httpOnly: true, maxAge: parseInt(req.query.expires_in)*1000 });
        res.redirect(tempSession.redirectTo || afterLoginPath);
      })
      .catch((err) => {
        next(err);
      });
  });
};
