module.exports = (router, encryptor) => {
  router.get('*', (req, res, next) => {
    // Check if cookie is there
    if(req.cookies['x-session'])
      encryptor.decrypt(req.cookies['x-session'])
        .then((user) => {
          req.user = JSON.parse(user);
        })
        .catch((err) => {
          res.cookie('x-session', 'Removing...', { maxAge: 0 });
        })
        .finally(() => {
          next();
        });
    else
      next();
  });
};
