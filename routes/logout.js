module.exports = (router, client, logoutPath, afterLogoutPath) => {
  router.get(logoutPath, (req, res) => {
    if(req.cookies['x-session'])
      res.cookie('x-session', 'Removing...', { maxAge: 0 });
    res.redirect(client.endSessionUrl({ redirect_uri: `${req.protocol}://${req.headers.host}${afterLogoutPath}` }));
  });
}
