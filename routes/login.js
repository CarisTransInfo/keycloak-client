const { v4: uuid } = require('uuid');
const { generators } = require('openid-client');
module.exports = (router, client, loginPath, ssoPath, cache) => {
  router.get(loginPath, (req, res) => {

    // Create temporary session
    const session = {
      id: uuid(),
      redirectTo: req.query.ReturnUrl || '/',
      nonce: generators.nonce()
    };
    res.cookie('x-session-temp', session.id, { maxAge: 5 * 60 * 1000, httpOnly: true });

    // Add nonce to cache
    cache.set(session.id, session, 5 * 60 * 1000); // keep nonce for 5 mins

    res.redirect(client.authorizationUrl({
      scope: 'openid email profile',
      nonce: session.nonce,
      redirect_uri: `${req.protocol}://${req.headers.host}${ssoPath}`
    }));
  });
}
