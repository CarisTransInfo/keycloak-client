const debug = require('debug')('keycloak-client:token:router')

module.exports = (router, tokenValidator) => {

  router.all('*', (req, res, next) => {

    // Check if Authorization header is present
    if (!req.get('Authorization')) {
      debug('Authorization header is missing');
      next();
      return;
    }

    // Check if token header is there
    const tokenStr = req.get('Authorization').split(' ');

    // Skip if there is no token
    if (!tokenStr || tokenStr.length < 2) {
      debug('Authorization header does not have a token');
      next();
      return;
    }

    // Check if token valid
    tokenValidator(tokenStr[1])
      .then(({user, decoded}) => {
        debug('Token is mapped to user and added to the request context');
        req.token = decoded;
        req.user = user;
      })
      .catch(() => {
        debug('Token is not valid, skipping');
      })
      .finally(() => {
        next();
      });
  });
};
