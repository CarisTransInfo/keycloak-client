const debug = require('debug')('keycloak-client:token:authorize');

function checkException(url, exceptions) {
  if(!exceptions || !exceptions.length)
    return false;

  for(let i = 0; i < exceptions.length; i++) {
    if(url.length === exceptions[i].length)
      return true;
    else if (url.length > exceptions[i].length && url.substr(0, exceptions[i].length) === exceptions[i])
      return true;
  }
  return false;
}

module.exports = (role, exceptions, redirectToLogin = false) => (req, res, next) => {
  // Check if this is login or sso url
  if (req.url.match(/^\/login/i) || req.url.match(/^\/sso/i)) {
    debug(`Url ${req.url} is exception from default authorization rules`);
    next();
  }

  // Apply exceptions
  if (checkException(req.url, exceptions)) {
    debug(`Url ${req.url} is exception from authorization rules`);
    next();
  }

  // If there is no known user, return 401
  else if (!req.user) {
    debug('User is unknown where required');
    if(!redirectToLogin)
      next({ status: 401, message: 'Unauthorized'});
    else
      res.redirect('/login?ReturnUrl=' + encodeURIComponent(req.originalUrl));
  }

  // If user exists but does not belong to a specified role, return 403
  else if (role && !req.user.roles?.includes(role)) {
    debug(`User is known, but it does not have an appropriate role '${role}'`);
    next({ status: 403, message: 'Forbidden'});
  }

  // If user is authorized
  else {
    debug('User is known' + (role ? ` and has a needed role '${role}'` : ''));
    next();
  }
}
