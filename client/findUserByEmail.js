const axios = require('axios');

module.exports = (getAdminToken, ssoAdminUrl) => async (email) => {
  const adminToken = await getAdminToken();
  const response = await axios.get(
    `${ssoAdminUrl.replace(/\/$/, '')}/users?email=${encodeURIComponent(email)}`,
    { headers: { 'Authorization': `Bearer ${adminToken}` } });
  return response.data;
}
