const axios = require('axios');

module.exports = (getAdminToken, findUserByEmail, ssoAdminUrl) => async (email, password) => {
  const adminToken = await getAdminToken();
  const usersWithSameEmail = await findUserByEmail(email);
  const user = usersWithSameEmail.filter(i=>i.email == email)?.[0];
  if(!user)
    throw { status: 404, message: 'Not Found' };

  return axios.put(
    `${ssoAdminUrl.replace(/\/$/, '')}/users/${user.id}/reset-password`,
    { type: 'password', value: password, temporary: false },
    { headers: { 'Authorization': `Bearer ${adminToken}` } })
    .then(() => {
      return true;
    })
    .catch((err) => {
      if(err.status === 400)
        throw { status: 400, message: 'Bad Request' }
      else
        throw err;
    });
}
