const axios = require('axios');
const querystring = require('querystring');

module.exports = (ssoUrl, client_secret = '', client_id = 'admin-cli') => (username, password) => {
  return axios.post(
    `${ssoUrl.replace(/\/$/, '')}/protocol/openid-connect/token`,
    querystring.stringify({
      client_id,
      grant_type: 'password',
      client_secret,
      username,
      password
    }),
    {
      headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    })
    .then(res => res.status === 200)
    .catch((err) => {
      if(err.response.status === 401)
        return false;
      else
        return Promise.reject(err.response);
    })
};
