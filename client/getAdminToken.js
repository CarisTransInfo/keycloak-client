const axios = require('axios');
const querystring = require('querystring');
const NodeCache = require('node-cache');
const cache = new NodeCache();

module.exports = (ssoMasterUrl, client_secret, client_id = 'admin-cli') => async () => {
  if (!cache.get('admin-token')) {
    const response = await axios.post(
      `${ssoMasterUrl.replace(/\/$/, '')}/protocol/openid-connect/token`,
      querystring.stringify({
        client_id,
        client_secret,
        grant_type: 'client_credentials'
      }),
      { headers: {'Content-Type': 'application/x-www-form-urlencoded'} });

    cache.set('admin-token', response.data.access_token, response.data.expires_in - 10);
  }

  // Return admin token
  return cache.get('admin-token');
}
