const axios = require('axios');

module.exports = (getAdminToken, ssoAdminUrl) => async (email, firstName, lastName, password) => {
  const adminToken = await getAdminToken();
  return axios.post(
    `${ssoAdminUrl.replace(/\/$/, '')}/users`,
    {
      firstName,
      lastName,
      email,
      emailVerified: true,
      enabled: true,
      username: email,
      credentials: [{ type: 'password', value: password, temporary: false }]
    },
    { headers: { 'Authorization': `Bearer ${adminToken}` } })
    .then((response) => {
      return response.status === 201;
    })
    .catch((err) => {
      if(err.status === 409)
        throw { status: 409, message: 'Conflict' }
      else
        throw err;
    });
}
