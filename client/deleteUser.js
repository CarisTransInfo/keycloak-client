const axios = require('axios');

module.exports = (getAdminToken, findUserByEmail, ssoAdminUrl) => async (email) => {
  const adminToken = await getAdminToken();
  const usersWithSameEmail = await findUserByEmail(email);
  const user = usersWithSameEmail.filter(i=>i.email == email)?.[0];
  if(!user)
    throw 'User does not exist';
  return axios.delete(
    `${ssoAdminUrl.replace(/\/$/, '')}/users/${user.id}`,
    { headers: { 'Authorization': `Bearer ${adminToken}` } }
  )
  .then(() => true)
  .catch((err) => {
    console.error(err);
    return false;
  });

}
