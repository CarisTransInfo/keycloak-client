
const changePasswordModule = require('./changePassword');
const createUserModule = require('./createUser');
const deleteUserModule = require('./deleteUser');
const findUserByEmailModule = require('./findUserByEmail');
const getAdminTokenModule = require('./getAdminToken');
const validateCredentialsModule = require('./validateCredentials');

module.exports = ({
  ssoUrl,
  masterSecret,
  masterClientId = 'admin-cli',
  realmSecret = '',
  realmClientId = 'admin-cli'
}) => {

  const ssoAdminUrl = ssoUrl.replace(/\/auth\/realms\//, '/auth/admin/realms/');
  const ssoMasterUrl = ssoUrl.replace(/\/auth\/realms\/\w+\/*/, '/auth/realms/master');

  const getAdminToken = getAdminTokenModule(ssoMasterUrl, masterSecret, masterClientId);
  const findUserByEmail = findUserByEmailModule(getAdminToken, ssoAdminUrl);
  const validateCredentials = validateCredentialsModule(ssoUrl, realmSecret, realmClientId);
  const createUser = createUserModule(getAdminToken, ssoAdminUrl);
  const changePassword = changePasswordModule(getAdminToken, findUserByEmail, ssoAdminUrl);
  const deleteUser = deleteUserModule(getAdminToken, findUserByEmail, ssoAdminUrl)

  return {
    findUserByEmail,
    createUser,
    validateCredentials,
    changePassword,
    deleteUser
  };


  // Init
  /*
  Issuer
    .discover(`${options.ssoUrl}/.well-known/uma2-configuration`)
    .then((keycloakIssuer) => {
      client = new keycloakIssuer.Client({
        client_id: 'admin-cli',
        response_types: ['token'],
      });
    })
    .catch((err) => {
      console.error(err);
    });

   */
};
