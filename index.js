module.exports = {
  router: require('./router'),
  authorize: require('./authorize'),
  client: require('./client')
}
