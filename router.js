const express = require('express');
const router = express.Router();
const { Issuer, KnownKeys } = require('openid-client');
const apiRouterModule = require('./routes/apiRouter');
const appRouterModule = require('./routes/appRouter');
const certFetcher = require('./services/certFetcher');
const tokenValidator = require('./services/tokenValidator');

module.exports = (options) => {
  // Check options
  if(!options)
    throw Error('You need to set options parameter');

  if(!options.ssoUrl)
    throw Error('ssoUrl is always required');

  (async() => {
    const keycloakIssuer = await Issuer .discover(`${options.ssoUrl}/.well-known/uma2-configuration`);
    const client = new keycloakIssuer.Client({
      client_id: options.clientId || 'docs',
      response_types: ['token'],
    });

    // Fetch certificate if not provided
    const cert = options.publicKey || await certFetcher(keycloakIssuer.jwks_uri);

    if(options.api)
      apiRouterModule(router, tokenValidator(cert));
    else
      appRouterModule(router, client, tokenValidator(cert), options);
  })()
    .catch((err) => {
      console.error(err);
    });

  return router;
};
